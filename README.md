## 介绍
 
本仓库用于提交龙蜥一刻-CentOS迁移到Anolis OS的PR。

龙蜥社区提供了龙蜥实验室，已经预装了CentOS操作系统，让您体验如何快速迁移到龙蜥操作系统。

## 活动详情与步骤

龙蜥一刻-CentOS迁移到Anolis OS的操作步骤，请参见[这里](https://gitee.com/anolis-challenge/summer2022/blob/master/%E9%BE%99%E8%9C%A5%E4%B8%80%E5%88%BB/%E8%BF%81%E7%A7%BB%E4%BD%93%E9%AA%8C%E4%BB%BB%E5%8A%A1/Centos%E8%BF%81%E7%A7%BB%E4%BB%BB%E5%8A%A1%E4%BD%93%E9%AA%8C%E6%B5%81%E7%A8%8B.md)。

注意：完成迁移后，必须将成果提交到本仓库，才能获得贡献值。